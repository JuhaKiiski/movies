import React from "react";

import { Movie } from "../api";

interface MovieListProps {
    movies: Movie[];
}

const MovieList: React.FunctionComponent<MovieListProps> = props => {
    return (
        <div className="movie-list">
            {props.movies.length > 0 &&
             <ul>
                 {props.movies.map(movie =>
                     <li key={movie.title}>
                         <span className="title">{movie.title} </span>
                         <span className="genres">{movie.genres.join(", ")}</span>
                     </li>
                 )}
             </ul>
            }
            {props.movies.length === 0 &&
             <p>No matches.</p>
            }
        </div>
    );
};

export default MovieList;
