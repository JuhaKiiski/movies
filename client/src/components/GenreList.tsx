import React from "react";

import { Genre } from "../api";

interface GenreListProps {
    genres: Genre[];
    onChange: (genres: Genre[]) => void;
}

interface GenreItemProps {
    genre: Genre;
    onChange: (event: React.FormEvent<HTMLInputElement>) => void;
}

const GenreItem: React.FunctionComponent<GenreItemProps> = props => {
    return (
        <li>
            <input
                type="checkbox"
                onChange={props.onChange}
                checked={props.genre.selected}
            /> {props.genre.title}
        </li>
    );
};

const GenreList: React.FunctionComponent<GenreListProps> = props => {
    const genreHandler = (genre: Genre) => (event: React.FormEvent<HTMLInputElement>) => {
        props.onChange(props.genres.map(g =>
            g === genre
            ? { ...genre, selected: event.currentTarget.checked }
            : g
        ));
    };

    return (
        <div className="genre-list">
            <ul>
                {props.genres.map(genre =>
                    <GenreItem
                        key={genre.title}
                        genre={genre}
                        onChange={genreHandler(genre)}
                    />
                )}
            </ul>
        </div>
    );
}

export default GenreList;
