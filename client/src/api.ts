import axios from "axios";

const apiUrl = process.env.REACT_APP_BACKEND_URL || "localhost:3000";

export interface Genre {
    title: string;
    selected: boolean;
}

export interface Movie {
    title: string;
    genres: string[];
}

export async function fetchGenres(): Promise<string[]> {
    try {
        const result = await axios.get(`${apiUrl}/api/genres`);
        return result.data;
    } catch (error) {
        console.error("Failed to fetch genres:", error);
        return [];
    }
}

export async function fetchMovies(genres: string[]): Promise<Movie[]> {
    try {
        const result = await axios.get(`${apiUrl}/api/movies/${genres.join("+")}`);
        return result.data;
    } catch (error) {
        console.error("Failed to fetch movies:", error);
        return [];
    }
}
