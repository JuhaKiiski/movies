import React, { useState, useEffect } from 'react';

import { Genre, Movie, fetchGenres, fetchMovies } from "./api";
import GenreList from "./components/GenreList";
import MovieList from "./components/MovieList";

function App() {
    const [genres, setGenres] = useState([] as Genre[]);
    const [movies, setMovies] = useState([] as Movie[]);

    useEffect(() => {
        fetchGenres().then(genres => setGenres(genres.map(genre => {
            return {
                title: genre,
                selected: false,
            }
        })));
    }, [setGenres]);

    useEffect(() => {
        const genreTitles = genres.filter(g => g.selected === true).map(g => g.title);
        if (genreTitles.length === 0) {
            setMovies([]);
        } else {
            fetchMovies(genreTitles).then(setMovies);
        }
    }, [genres, setMovies]);

    return (
        <div className="App">
            <GenreList
                genres={genres}
                onChange={setGenres}
            />
            <MovieList
                movies={movies}
            />
        </div>
    );
}

export default App;
