import React from "react";
import { render, waitForElement } from "@testing-library/react";

import App from "./App";

describe("<App />", () => {
    test("UI renders correctly", async () => {
        const component = render(<App />);
        component.rerender(<App />);

        const genres = component.container.querySelector(".genre-list");
        const movies = component.container.querySelector(".movie-list");

        expect(genres).toBeTruthy();
        expect(movies).toBeTruthy();
    });
});
