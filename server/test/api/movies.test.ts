import request from "supertest";
import app from "../../src/app";

describe("Movie API", () => {
    it("Find movies by single genre", () => {
        return request(app).get("/api/movies/crime")
            .expect("Content-Type", /json/)
            .expect(200, [
                {
                    title: "Kummisetä",
                    genres: ["crime", "drama"],
                },
                {
                    title: "Kummisetä osa II",
                    genres: ["crime", "drama"],
                },
                {
                    title: "Yön ritari",
                    genres: ["action", "crime", "drama"],
                },
                {
                    title: "Pulp Fiction: Tarinoita väkivallasta",
                    genres: ["crime", "drama"],
                },
            ]);
    });

    it("Find movies by multiple genres", () => {
        return request(app).get("/api/movies/crime+action")
            .expect("Content-Type", /json/)
            .expect(200, [
                {
                    title: "Yön ritari",
                    genres: ["action", "crime", "drama"],
                },
            ]);
    });

    it("Find movies by non-existing genre", () => {
        return request(app).get("/api/movies/foobar")
            .expect("Content-Type", /json/)
            .expect(200, []);
    });

    it("Known genres", () => {
        return request(app).get("/api/genres")
            .expect("Content-Type", /json/)
            .expect(200, [
                'action', 'adventure',
                'biography', 'crime',
                'drama', 'fantasy',
                'history', 'western',
            ]);
    });
});
