import express from "express";

import movies from "../movies/movies";

const apiRouter = express.Router();

apiRouter.get("/movies/:genres", (request, response) => {
    response.status(200).json(movies.findByGenres(request.params.genres.split("+")));
});

apiRouter.get("/genres", (_request, response) => {
    response.status(200).json(movies.knownGenres());
});

export default apiRouter;
