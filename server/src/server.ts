require('dotenv').config()

import app from "./app";

const port = app.get("port");

const server = app.listen(port, () => {
    console.log("Running on port %d", port);
});

export default server;
