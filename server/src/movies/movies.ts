import { Movie, Source } from "./types";
import staticTestingSource from "./static_testing";

/**
 * An array of sources to use. The results from all sources will be
 * collated into a single response by the exported functions here.
 *
 * See `static_testing.ts` for an example.
 */
const sources: Source[] =
    process.env.NODE_ENV === "testing"
        ? [
            staticTestingSource,
        ]
        : [
            // Production sources...
        ];

/**
 * Retrieve movie suggestions that have the given genres.
 */
function findByGenres(genres: string[]): Movie[] {
    let result: Movie[] = [];
    sources.map(api => api.moviesByGenres(genres))
        .forEach(list => list.forEach(movie => {
            if (!result.find(m => m.title === movie.title)) {
                result.push(movie);
            }
        }));
    return result;
}

/**
 * Retrieve a list of all known genres.
 */
function knownGenres(): string[] {
    const result: string[] = [];
    sources.map(api => api.knownGenres().forEach(genre => {
        if (!result.includes(genre)) {
            result.push(genre);
        }
    }));
    return result;
}

export default {
    findByGenres,
    knownGenres,
};
