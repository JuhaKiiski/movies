/**
 * A movie. For the excercise we only need title and an array of genres.
 */
export interface Movie {
    title: string;
    genres: string[];
}

/**
 * A movie info source (such as the IMDB API for example).
 *
 * The source is responsible for limiting the results; the server will
 * just pass everything to the user.
 */
export interface Source {
    /**
     * Returns an array of movies that have all of the genres given as
     * an argument.
     */
    moviesByGenres: (genres: string[]) => Movie[];

    /** Returns an array of all valid genres. */
    knownGenres: () => string[];
}
