import { Source, Movie } from "./types";

const movies: Movie[] = [
    {
        title: "Rita Hayworth - avain pakoon",
        genres: ["drama"],
    },
    {
        title: "Kummisetä",
        genres: ["crime", "drama"],
    },
    {
        title: "Kummisetä osa II",
        genres: ["crime", "drama"],
    },
    {
        title: "Yön ritari",
        genres: ["action", "crime", "drama"],
    },
    {
        title: "Valamiesten ratkaisu",
        genres: ["drama"],
    },
    {
        title: "Schindlerin lista",
        genres: ["biography", "drama", "history"],
    },
    {
        title: "Taru sormusten herrasta: Kuninkaan paluu",
        genres: ["adventure", "drama", "fantasy"],
    },
    {
        title: "Pulp Fiction: Tarinoita väkivallasta",
        genres: ["crime", "drama"],
    },
    {
        title: "Hyvät, pahat ja rumat",
        genres: ["western"],
    },
    {
        title: "Taru sormusten herrasta: Sormuksen ritarit",
        genres: ["action", "adventure", "drama", "fantasy"],
    },
]

/**
 * A source with a static array of testing data. This makes it easier
 * to write tests (of course not tests for the APIs themselves).
 */
const staticTestingSource: Source = {
    moviesByGenres: genres => {
        return movies.filter(m => genres.every(genre => m.genres.includes(genre)));
    },

    knownGenres: () => {
        const result: string[] = [];
        movies.forEach(movie => movie.genres.forEach(genre => {
            if (!result.includes(genre)) {
                result.push(genre);
            }
        }));
        return result.sort();
    },
};

export default staticTestingSource;
