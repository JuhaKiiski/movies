import express from "express";
import cors from "cors";

import apiRouter from "./controllers/api";

const app = express();
app.use(cors());

app.set("port", process.env.PORT || 3000);

app.use("/api", apiRouter);

export default app;
